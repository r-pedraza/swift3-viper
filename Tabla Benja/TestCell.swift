//
//  TestCell.swift
//  Tabla Benja
//
//  Created by Raúl Pedraza on 9/2/17.
//  Copyright © 2017 Raúl Pedraza. All rights reserved.
//

import UIKit

class TestCell: UITableViewCell {

    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var title: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
