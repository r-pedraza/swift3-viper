//
//  BreakdownOfExpensesInteractorInputProtocol.swift
//  Tabla Benja
//
//  Created by Raúl Pedraza on 5/2/17.
//  Copyright © 2017 Raúl Pedraza. All rights reserved.
//

import Foundation

protocol BreakdownOfExpensesInteractorInputProtocol {
    func objectAtIndex(indexPath:IndexPath) -> Any
    func numberOfRowsInSection(section:Int) -> Int
    func numberOfSections() -> Int
    func createViewModels()
    func addImage()
    func deleteImage()
    func addProvider()
    func deleteProvider()
}
