//
//  BreakdownOfExpensesInteractor.swift
//  Tabla Benja
//
//  Created by Raúl Pedraza on 5/2/17.
//  Copyright © 2017 Raúl Pedraza. All rights reserved.
//

import UIKit

class BreakdownOfExpensesInteractor: NSObject,BreakdownOfExpensesInteractorInputProtocol,BreakdownOfExpensesInteractorOutputProtocol {
    var viewModels:Array<Any>!
    var presenter:BreakdownOfExpensesInteractorOutputProtocol!
    
    override init() {
        viewModels = Array<Any>()
    }
    
    //MARK : BreakdownOfExpensesInteractorInputProtocol
    
    func objectAtIndex(indexPath:IndexPath) -> Any {
        return viewModels[indexPath.section]
    }
    
    func numberOfSections() -> Int {
        return 2
    }
    
    func numberOfRowsInSection(section: Int) -> Int {
        return 1;
    }
    
        func createViewModels() {
            let imageVM = ImageViewModel()
            imageVM.images?.append("Image 1")
            imageVM.images?.append("Image 2")
            
            let providerVM = ProviderViewModel()
            providerVM.providers.append("Provider 1")
            providerVM.providers.append("Provider 2")
            
            viewModels.append(imageVM)
            viewModels.append(providerVM)
        }
        
        func addImage() {
            let vm = objectAtIndex(indexPath:IndexPath(row: 0, section: 0)) as? ImageViewModel
            if let number = vm?.images.count{
                vm?.images.append("New Image \(number+1)")
            }
        }
        
        func deleteImage() {
            let vm = objectAtIndex(indexPath:IndexPath(row: 0, section: 0)) as? ImageViewModel
            if let images = vm?.images {
                if images.count > 0 {
                    vm?.images.removeLast()
                }
            }
        }
        
        func addProvider() {
            let vm = objectAtIndex(indexPath:IndexPath(row: 0, section: 1)) as? ProviderViewModel
            if let number = vm?.providers.count {
                vm?.providers.append("New Provider \(number+1)")
            }
        }
        
        func deleteProvider() {
            let vm = objectAtIndex(indexPath:IndexPath(row: 0, section: 1)) as? ProviderViewModel
            if let providers = vm?.providers {
                if providers.count > 0 {
                    vm?.providers.removeLast()
                }
            }
        }
}


