//
//  ProvidersTableViewHelper.swift
//  Tabla Benja
//
//  Created by Raúl Pedraza on 7/2/17.
//  Copyright © 2017 Raúl Pedraza. All rights reserved.
//

import UIKit

class ProvidersTableViewHelper: NSObject,UITableViewDataSource,UITableViewDelegate {
    
    var providers:Array<String>!
    init(providers:Array<String>) {
        self.providers = providers
    }
    
    //MARK:UITableViewDataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return providers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TestCellIdentifier", for: indexPath) as! TestCell

        cell.title.text = providers[indexPath.row]
    
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    //MARK:UITableViewDelegate
    
}
