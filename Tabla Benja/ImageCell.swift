//
//  ImageCell.swift
//  Tabla Benja
//
//  Created by Raúl Pedraza on 7/2/17.
//  Copyright © 2017 Raúl Pedraza. All rights reserved.
//

import UIKit

class ImageCell: UITableViewCell {

    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    @IBOutlet weak var imagesTableView: UITableView!
    var delegate: UITableViewDelegate!
    var dataSource: UITableViewDataSource!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let testNib = UINib.init(nibName: "TestCell", bundle: nil)
        imagesTableView.register(testNib, forCellReuseIdentifier: "TestCellIdentifier")
        self.imagesTableView.rowHeight = UITableViewAutomaticDimension
        self.imagesTableView.estimatedRowHeight = 80;
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
