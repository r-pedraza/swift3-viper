//
//  BreakdownOfExpensesPresenterProtocol.swift
//  Tabla Benja
//
//  Created by Raúl Pedraza on 5/2/17.
//  Copyright © 2017 Raúl Pedraza. All rights reserved.
//

import UIKit

protocol BreakdownOfExpensesPresenterProtocol {
    func load()
    func addImage()
    func deleteImage()
    func addProvider()
    func deleteProvider()
}
