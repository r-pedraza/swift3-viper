//
//  BreakdownOfExpensesConfigurator.swift
//  Tabla Benja
//
//  Created by Raúl Pedraza on 9/2/17.
//  Copyright © 2017 Raúl Pedraza. All rights reserved.
//

import UIKit

class BreakdownOfExpensesConfigurator: NSObject {
    
    //MARK: Shared Instance
    
    static let sharedInstance : BreakdownOfExpensesConfigurator = {
        let instance = BreakdownOfExpensesConfigurator()
        return instance
    }()
    
    
    func configure(viewController:BreakdownOfExpensesView) {
        let interactor = BreakdownOfExpensesInteractor()
        let presenter  = BreakdownOfExpensesPresenter()

        interactor.presenter     = presenter
        presenter.interactor     = interactor
        presenter.view           = viewController
        viewController.presenter = presenter
    }
}
