//
//  ProviderCell.swift
//  Tabla Benja
//
//  Created by Raúl Pedraza on 7/2/17.
//  Copyright © 2017 Raúl Pedraza. All rights reserved.
//

import UIKit

class ProviderCell: UITableViewCell {

    @IBOutlet weak var providersTableView: UITableView!
    
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        let testNib = UINib.init(nibName: "TestCell", bundle: nil)
        providersTableView.register(testNib, forCellReuseIdentifier: "TestCellIdentifier")
        self.providersTableView.rowHeight = UITableViewAutomaticDimension
        self.providersTableView.estimatedRowHeight = 80;
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
