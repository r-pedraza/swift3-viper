//
//  BreakdownOfExpensesDrawer.swift
//  Tabla Benja
//
//  Created by Raúl Pedraza on 5/2/17.
//  Copyright © 2017 Raúl Pedraza. All rights reserved.
//

import UIKit

class BreakdownOfExpensesDrawer: NSObject {
    
    static func cellForImage(image:ImageViewModel!, tableView:UITableView!,indexPath:IndexPath)->ImageCell{
    
     let cell = tableView.dequeueReusableCell(withIdentifier: "ImageCellIdentifier", for: indexPath) as? ImageCell
 
        let helper = ImagesTableViewHelper(images:image.images)
    cell?.imagesTableView.delegate   = helper
    cell?.imagesTableView.dataSource = helper
    cell?.imagesTableView.reloadData()
    let height =  image.images.count * 44
    cell?.heightConstraint.constant = CGFloat(height)
    cell?.layoutIfNeeded()

    
        return cell!
    }
    
    static func cellForProvider(provider:ProviderViewModel!, tableView:UITableView!,indexPath:IndexPath)->ProviderCell{
          let cell = tableView.dequeueReusableCell(withIdentifier: "ProviderCellIdentifier", for: indexPath) as? ProviderCell
        
        let helper = ProvidersTableViewHelper(providers:provider.providers)
        cell?.providersTableView.delegate   = helper
        cell?.providersTableView.dataSource = helper
        cell?.providersTableView.reloadData()
        let height =  provider.providers.count * 44
        cell?.heightConstraint.constant = CGFloat(height)
        cell?.layoutIfNeeded()
        return cell!
    }

}
