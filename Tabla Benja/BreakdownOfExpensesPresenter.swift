//
//  BreakdownOfExpensesPresenter.swift
//  Tabla Benja
//
//  Created by Raúl Pedraza on 5/2/17.
//  Copyright © 2017 Raúl Pedraza. All rights reserved.
//

import UIKit

class BreakdownOfExpensesPresenter: NSObject,BreakdownOfExpensesInteractorOutputProtocol,BreakdownOfExpensesPresenterProtocol,UITableViewDataSource,UITableViewDelegate{
    weak var view :BreakdownOfExpensesViewProtocol!
    var interactor:BreakdownOfExpensesInteractorInputProtocol!
    
    //MARK : BreakdownOfExpensesPresenterProtocol
    func load()  {
        self.interactor.createViewModels()
    }
  
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.interactor.numberOfSections()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.interactor.numberOfRowsInSection(section: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let object = interactor.objectAtIndex(indexPath: indexPath)
        return BreakdownOfExpensesFactoryCell.cellForObject(object:object, tableView: tableView,indexPath:indexPath)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return UITableViewAutomaticDimension

        }
        else if indexPath.section == 1 {
            return UITableViewAutomaticDimension
        }else{
        return 0
        }
    }
    
    func addImage() {
        self.interactor.addImage()
        self.view.reloadData()
    }
    
    func deleteImage() {
        self.interactor.deleteImage()
        self.view.reloadData()
    }
    
    func addProvider() {
        self.interactor.addProvider()
        self.view.reloadData()
    }
    
    func deleteProvider() {
        self.interactor.deleteProvider()
        self.view.reloadData()
    }
}
