//
//  BreakdownOfExpensesView.swift
//  Tabla Benja
//
//  Created by Raúl Pedraza on 5/2/17.
//  Copyright © 2017 Raúl Pedraza. All rights reserved.
//

import UIKit

class BreakdownOfExpensesView: UIViewController,BreakdownOfExpensesViewProtocol{
    @IBOutlet weak var tableView: UITableView!
    var presenter:BreakdownOfExpensesPresenterProtocol?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        BreakdownOfExpensesConfigurator.sharedInstance.configure(viewController: self)
    }
 
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.presenter?.load()
        tableView.delegate           = self.presenter as? UITableViewDelegate
        tableView.dataSource         = self.presenter as? UITableViewDataSource
        tableView.rowHeight          = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 44;
        registerCells()
    }
    
    
    func registerCells() {
        let imageNib = UINib.init(nibName: "ImageCell", bundle: nil)
        tableView.register(imageNib, forCellReuseIdentifier: "ImageCellIdentifier")
        let providerNib = UINib.init(nibName: "ProviderCell", bundle: nil)
        tableView.register(providerNib, forCellReuseIdentifier: "ProviderCellIdentifier")
    }
    
    @IBAction func addImageAction(_ sender: Any) {
        self.presenter?.addImage()
    }
  
    @IBAction func deleteImageAction(_ sender: Any) {
        self.presenter?.deleteImage()
    }

    @IBAction func addProviderAction(_ sender: Any) {
        self.presenter?.addProvider()
    }

    @IBAction func deleteProviderAction(_ sender: Any) {
    self.presenter?.deleteProvider()
    }
    
    func reloadData()  {
        UIView.animate(withDuration: 1.0, animations: { 
            self.tableView.reloadData()
        }) { (_) in
            self.tableView.layoutIfNeeded()
        }
    }
 
}
