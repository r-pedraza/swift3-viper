//
//  BreakdownOfExpensesFactoryCellFactoryCell.swift
//  Tabla Benja
//
//  Created by Raúl Pedraza on 5/2/17.
//  Copyright © 2017 Raúl Pedraza. All rights reserved.
//

import UIKit

class BreakdownOfExpensesFactoryCell: NSObject {
    
    static func cellForObject(object:Any?, tableView:UITableView?,indexPath:IndexPath) -> UITableViewCell{
        if let object = object, let tableView = tableView {
            
            if object is ProviderViewModel {
             return BreakdownOfExpensesDrawer.cellForProvider(provider:object as? ProviderViewModel, tableView: tableView,indexPath: indexPath)
            }
            
            if object is ImageViewModel {
               return BreakdownOfExpensesDrawer.cellForImage(image: object as? ImageViewModel, tableView: tableView,indexPath:indexPath)
            }
        }
    return UITableViewCell()
    }

}
